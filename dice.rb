#!/usr/bin/env ruby
#encoding: utf-8

require "rubyhelper"
require "colorize"

class Dice
  MIN = [7, 10]
  MAX = [15, 18]
  AVE = [10, 13.5]
  def initialize n=6
    @n = n.to_i
    self.roll!
  end

  def self.rolln(ndices, nkeep=ndices)
    array = Array.new(ndices){ Dice.new.roll }
    return array.maxs(nkeep)
  end

  def self.get_carac
    return (Array.new(6) { Dice.rolln(4,3).sum.to_s.static(2, ' ', :front) })
  end

  def self.puts_n_carac_series(n=3, cheat=false)
    puts "-----------------------------------------------------------"
    puts "    SERIE.  C    C    C    C    C    C => [ AVER , MN , MX]"
    puts "-----------------------------------------------------------"

    n.min(1).max(99).times do |i|
      averagef = 0
      serie = []
      loop do
        serie = Dice.get_carac.sort.reverse
        averagef = serie.averagef.to_f
        break if cheat == false or averagef >= AVE[1]
      end
      min = serie.min.to_i
      max = serie.max.to_i

      mincolor = (min <= MIN[0] ? :red : (min >= MIN[1] ? :green : :white))
      maxcolor = (max <= MAX[0] ? :red : (max >= MAX[1] ? :green : :white))
      avecolor = (averagef <= AVE[0] ? :red : (averagef >= AVE[1] ? :green : :white))
      puts (i+1).to_s.static(3) + " SERIE. " + serie.join(" | ") \
	+ " => [" \
	+ "#{serie.averagef.to_s.static(4, ' ', :front)}".colorize(avecolor) \
	+ " , " \
	+ "#{serie.min.to_s.static(2, ' ', :front)}".colorize(mincolor) \
	+ " , " \
	+ "#{serie.max.to_s.static(2, ' ', :front)}".colorize(maxcolor) \
	+ " ]"
    end
  end

  def roll
    return rand(1..@n)
  end

  def roll!
    return @v = self.roll
  end

  def to_i
    return @v || self.roll
  end

  def to_s
    return @v.to_s
  end

end
